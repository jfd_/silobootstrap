/// SiloBootstrap
///
/// Lite environment that emulates the full Silo-suite.
///
(function(environment) {
  var SiloBootstrap;

  if (typeof module !== "undefined") {
    module.exports = SiloBootstrap;
  }

  // Module exports
  SiloBootstrap = {};
  SiloBootstrap.exractModuleExports = exractModuleExports;
  SiloBootstrap.utfResource = utfResource;
  SiloBootstrap.jsonResource = jsonResource;
  SiloBootstrap.base64Resource = base64Resource;
  SiloBootstrap.defineEnum = defineEnum;


  // Macros
  environment.SiloBootstrap = SiloBootstrap;
  environment.$$ = "eval(SiloBootstrap.exractModuleExports(this.constructor));";
  environment.$$enum = defineEnum;
  environment.$$utf = utfResource;
  environment.$$json = jsonResource;
  environment.$$base64 = base64Resource;

  var _resRe = /\/\*!?(?:\@preserve)?[ \t]*(?:\r\n|\n)([\s\S]*?)(?:\r\n|\n)[ \t]*\*\//;


  function exractModuleExports(fn) {
    var returns;
    var result;
    var idx;

    if (typeof fn !== "function") {
      throw new Error("Expected fn");
    }

    returns = parseReturns(fn);

    if (returns.defaults) {
      result = [];
      for (idx = 0; idx < returns.symbols.length; idx++) {
        result.push(returns.defaults + "." +
                    returns.symbols[idx] + "=" +
                    returns.symbols[idx]);
      }
      return "(" + result.join(",") + "," + returns.defaults + ")";
    }

    result = [];

    for (idx = 0; idx < returns.symbols.length; idx++) {
      result.push(returns.symbols[idx] + ":" + returns.symbols[idx]);
    }

    return "({" + result.join(",") + "})";
  }


  function utfResource(fn) {
    return resource(fn);
  }


  function jsonResource(fn, path) {
    var data;
    data = resource(fn);
    return typeof path == "undefined" ? data : data[path];
  }


  function base64Resource(fn) {
    return atob(resource(fn));
  }


  function defineEnum(definition) {
    return definition;
  }


  function isLinebreak(c) {
    return !(c !== 10 && c !== 13 && c !== 8232 && c !== 8233);
  }


  function isWhitespace(c) {
    return (c == " " || c == "\t");
  }


  function isIdent(c) {
    return (c == "$") || (c == "_") || (c >= "0" && c <= "9") ||
           ((c >= "a" && c <= "z") || (c >= "A" && c <= "Z"));
  }


  function resource(fn) {
    var result;

    if (typeof fn !== "function" && typeof fn.constructor !== "function") {
      throw new TypeError("Expected 'fn' to be a function or object");
    }

    result = _resRe.exec(typeof fn == "function" ? fn.toString()
                                                 : fn.constructor.toString());

    if (!result) {
      throw new TypeError("Multiline comment missing.");
    }

    return result[1];
  }


  function parseReturns(fn) {
    var tokens;
    var symbols;
    var exports;
    var scope;
    var idx;
    var type;
    var val;
    var base;

    symbols = [];
    tokens = fn.toString();
    scope = 0;
    idx = -1;
    type = null;
    val = null;


    function ident(init) {
      var c;
      var old;
      type = "ident";
      val = init;
      while ((c = tokens[++idx])) {
        if (isIdent(c)) {
          val += c;
        } else {
          if (c == ":") {
            type = "label";
          } else if (peekChar() == ":") {
            old = val;
            next();
            type = "label";
            val = old;
          } else {
            idx--;
          }
          return true;
        }
      }
      return true;
    }


    function identifiers() {
      var parans;
      var scopes;
      var comma;

      parans = 0;
      scopes = 0;
      comma = true;

      while (next()) {
        if (type == ";") {
          return;
        }

        if (type == "{") {
          scopes++;
          continue;
        }

        if (type == "}") {
          scopes--;
          continue;
        }

        if (type == "(") {
          parans++;
          continue;
        }

        if (type == ")") {
          if (parans == 0) {
            return;
          }
          parans--;
          continue;
        }

        if (type == ",") {
          comma = true;
        }

        if (comma && type == "ident") {
          symbols.push(val);
          comma = false;
        }
      }
    }


    function peekChar() {
      var i;
      var c;

      i = idx;

      while ((c = tokens[++i]) && isWhitespace(c));

      return c;
    }


    function nextChar() {
      while ((c = tokens[++idx]) && isWhitespace(c));
    }


    function next() {
      var c;
      var result;
      result = ""

      while ((c = tokens[++idx]) && isWhitespace(c));

      if (!c) return false;

      if (c == "$" || c == "_" ||
          ((c >= "a" && c <= "z") ||
          (c >= "A" && c <= "Z"))) {
        return ident(c);
      }

      type = c;
      val = c;

      return true;
    }


    function skipComment(tokens, idx) {
      var c;

      if (!(c = tokens[idx + 1])) {
        return;
      }

      if (c == "*") {
        idx = tokens.indexOf("*/", idx + 1);
        if (idx == -1) {
          throw new Error("Unterminated comment");
        }
        idx += 2;
      } else if (c == "/") {
        while ((c = tokens.charCodeAt(++idx)) && !isLinebreak(c));
      }
    }

    while ((val = tokens[++idx])) if (val == "{") {idx++; break; }

    while (next()) {

      if (type == "/") {
        skipComment(tokens, idx);
        continue;
      }

      if (type == "{") {
        scope++;
        continue;
      }

      if (type == "}") {
        scope--;
        continue;
      }

      if (scope == 0 && (type == "label" && val == "exports")) {
        next();
        if (val == "(" || val == "var" || val == "const") {
          identifiers();
        } else if (val == "function") {
          next();
          if (type == "ident") {
            symbols.push(val);
          }
        } else {
          symbols.push(val);
        }
      }

      if (scope == 0 && (type == "label" && val == "defaults")) {
        next();
        if (val == "(") {
          throw new Error("Only one base is accepted");
        }
        if (type == "ident") {
          base = val;
        }
      }
    }

    return { symbols: symbols, defaults: base};
  }

})(typeof global == "undefined" ? this : global);
